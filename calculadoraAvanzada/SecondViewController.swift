//
//  SecondViewController.swift
//  calculadoraAvanzada
//
//  Created by David Alejo Apaza on 4/26/21.
//  Copyright © 2021 David Alejo Apaza. All rights reserved.
//


import UIKit

class SecondViewController: UIViewController {

    @IBOutlet weak var txtNumero: UITextField!
    var numeroAnterior:Double = 0
    var numeroActual:Double = 0
    var operacion:String = ""

    func Operacion(){
        numeroAnterior = numeroActual
        numeroActual = 0
        txtNumero.text = ""
        print("numero actual: \(numeroActual)")
        print("numero anterior: \(numeroAnterior)")
    }
    
    func Numero(){
        numeroActual = 0
        if(txtNumero.text!.count > 0){
            numeroActual = Double(txtNumero.text!)!
        }
        print("numero actual: \(numeroActual)")
        print("numero anterior: \(numeroAnterior)")
    }
    
    @IBAction func btnDividir(_ sender: Any) {
        operacion = "/"
        Operacion()
    }
    @IBAction func btnMultiplicar(_ sender: Any) {
        operacion = "*"
        Operacion()
    }
    @IBAction func btnRestar(_ sender: Any) {
        operacion = "-"
        Operacion()
    }
    @IBAction func btnSumar(_ sender: Any) {
        operacion = "+"
        Operacion()
    }
    @IBAction func btnFactorial(_ sender: Any) {
        operacion = "n!"
        Operacion()
    }
    @IBAction func btnSumatoria(_ sender: Any) {
        operacion = "n+"
        Operacion()
    }
    @IBAction func btnPotenciaCuadrada(_ sender: Any) {
        operacion = "pt2"
        Operacion()
    }
    @IBAction func btnPotenciaCubica(_ sender: Any) {
        operacion = "pt3"
        Operacion()
    }
    @IBAction func btnPotenciaX(_ sender: Any) {
        operacion = "ptx"
        Operacion()
    }
    @IBAction func btnRaizCuadrada(_ sender: Any) {
        operacion = "rz2"
        Operacion()
    }
    @IBAction func btnRaizCubica(_ sender: Any) {
        operacion = "rz3"
        Operacion()
    }
    @IBAction func btnRaizX(_ sender: Any) {
        operacion = "rzx"
        Operacion()
    }
    @IBAction func btnSimboloNegativo(_ sender: Any) {
        operacion = "neg"
        Operacion()
    }
    @IBAction func btnValorAbsoluto(_ sender: Any) {
        operacion = "abs"
        Operacion()
    }
    
    
    @IBAction func btnLimpiar(_ sender: Any) {
        numeroAnterior = 0
        numeroActual = 0
        txtNumero.text = ""
        operacion = ""
    }
    
    @IBAction func btnIgual(_ sender: Any) {
        if(operacion.count > 0){
            var resultado = 0.0
            
            switch operacion {
            case "+":
                resultado = numeroAnterior + numeroActual
                txtNumero.text = String(resultado)
                numeroActual = resultado
            
            case "-":
                resultado = numeroAnterior - numeroActual
                txtNumero.text = String(resultado)
                numeroActual = resultado
            case "*":
                resultado = numeroAnterior * numeroActual
                txtNumero.text = String(resultado)
                numeroActual = resultado
            case "/":
                if(numeroActual != 0){
                    resultado = numeroAnterior / numeroActual
                    txtNumero.text = String(resultado)
                    numeroActual = resultado
                }else{
                    print("Error: División entre 0")
                    numeroAnterior = 0
                    numeroActual = 0
                    txtNumero.text = ""
                    operacion = ""
                }
            case "n!":
                resultado = 1
                for index in 1...Int(numeroAnterior) {
                    resultado *= Double(index)
                }
                txtNumero.text = String(resultado)
                numeroActual = resultado
                
            case "n+":
                resultado = 0
                for index in 1...Int(numeroAnterior) {
                    resultado += Double(index)
                }
                txtNumero.text = String(resultado)
                numeroActual = resultado
            case "pt2":
                resultado = pow(numeroAnterior,2)
                txtNumero.text = String(resultado)
                numeroActual = resultado
            case "pt3":
                resultado = pow(numeroAnterior,3)
                txtNumero.text = String(resultado)
                numeroActual = resultado
            case "ptx":
                resultado = pow(numeroAnterior,numeroActual)
                txtNumero.text = String(resultado)
                numeroActual = resultado
            case "rz2":
                resultado = pow(numeroAnterior,1/2)
                txtNumero.text = String(resultado)
                numeroActual = resultado
            case "rz3":
                resultado = pow(numeroAnterior,1/3)
                txtNumero.text = String(resultado)
                numeroActual = resultado
            case "rzx":
                resultado = pow(numeroAnterior,1/numeroActual)
                txtNumero.text = String(resultado)
                numeroActual = resultado
            case "abs":
                resultado = abs(numeroAnterior)
                txtNumero.text = String(resultado)
                numeroActual = resultado
            case "neg":
                resultado = numeroAnterior * -1
                txtNumero.text = String(resultado)
                numeroActual = resultado
            default:
                print("Error")
            }
        }
    }
    
    //numeros
    
    @IBAction func btnNumero0(_ sender: Any) {
        txtNumero.text? += "0"
        Numero()
    }
    
    @IBAction func btnNumero1(_ sender: Any) {
        txtNumero.text? += "1"
        Numero()
    }
    @IBAction func btnNumero2(_ sender: Any) {
        txtNumero.text? += "2"
        Numero()
    }
    @IBAction func btnNumero3(_ sender: Any) {
        txtNumero.text? += "3"
        Numero()
    }
    @IBAction func btnNumero4(_ sender: Any) {
        txtNumero.text? += "4"
        Numero()
    }
    @IBAction func btnNumero5(_ sender: Any) {
        txtNumero.text? += "5"
        Numero()
    }
    @IBAction func btnNumero6(_ sender: Any) {
        txtNumero.text? += "6"
        Numero()
    }
    @IBAction func btnNumero7(_ sender: Any) {
        txtNumero.text? += "7"
        Numero()
    }
    @IBAction func btnNumero8(_ sender: Any) {
        txtNumero.text? += "8"
        Numero()
    }
    @IBAction func btnNumero9(_ sender: Any) {
        txtNumero.text? += "9"
        Numero()
    }
    @IBAction func btnPunto(_ sender: Any) {
        txtNumero.text? += "."
        Numero()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }


}

